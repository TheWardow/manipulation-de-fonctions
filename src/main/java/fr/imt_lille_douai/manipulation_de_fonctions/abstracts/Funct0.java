package fr.imt_lille_douai.manipulation_de_fonctions.abstracts;

import fr.imt_lille_douai.manipulation_de_fonctions.interfaces.Function;

public abstract class Funct0 implements Function {
	
	protected double value = 0.0;
	
	public double getValue() {
		return value;
	}
}
