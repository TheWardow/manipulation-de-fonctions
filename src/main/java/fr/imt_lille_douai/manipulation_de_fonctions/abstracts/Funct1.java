package fr.imt_lille_douai.manipulation_de_fonctions.abstracts;

import fr.imt_lille_douai.manipulation_de_fonctions.interfaces.Function;

public abstract class Funct1 implements Function {
	protected Function f;
	
	public Funct1(Function f) {
		this.f = f;
	}

	public Function getF() {
		return f;
	}

	public void setF(Function f) {
		this.f = f;
	}
}
