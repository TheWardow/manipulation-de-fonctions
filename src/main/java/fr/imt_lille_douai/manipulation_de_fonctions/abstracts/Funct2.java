package fr.imt_lille_douai.manipulation_de_fonctions.abstracts;

import fr.imt_lille_douai.manipulation_de_fonctions.interfaces.Function;

public abstract class Funct2 implements Function {
	protected Function f1;
	protected Function f2;
	
	public Funct2(Function f1, Function f2) {
		this.f1 = f1;
		this.f2 = f2;
	}

	public Function getF1() {
		return f1;
	}

	public void setF1(Function f) {
		this.f1 = f;
	}

	public Function getF2() {
		return f2;
	}

	public void setF2(Function f2) {
		this.f2 = f2;
	}
}
