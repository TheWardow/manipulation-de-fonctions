package fr.imt_lille_douai.manipulation_de_fonctions.functions;

import fr.imt_lille_douai.manipulation_de_fonctions.abstracts.Funct1;
import fr.imt_lille_douai.manipulation_de_fonctions.interfaces.Function;
import fr.imt_lille_douai.manipulation_de_fonctions.operateurs.Div;
import fr.imt_lille_douai.manipulation_de_fonctions.operateurs.Minus;
import fr.imt_lille_douai.manipulation_de_fonctions.operateurs.Mul;
import fr.imt_lille_douai.manipulation_de_fonctions.variables.Val;
import fr.imt_lille_douai.manipulation_de_fonctions.variables.Var;

public class Inv extends Funct1 {

	public Inv(Function f) {
		super(f);
		// TODO Auto-generated constructor stub
	}

	public double getValue() {
		return -1.0 / getF().getValue();
	}
	
	public String toString() {
		return "1/(" + getF() + ")";
	}

	public Function getDiff(Var x) {
		return new Mul(new Val(-1.0), new Mul(f.getDiff(x), new Inv(new Sqr(f))));
	}

}
