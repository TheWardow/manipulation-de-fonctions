package fr.imt_lille_douai.manipulation_de_fonctions.functions;

import fr.imt_lille_douai.manipulation_de_fonctions.abstracts.Funct1;
import fr.imt_lille_douai.manipulation_de_fonctions.interfaces.Function;
import fr.imt_lille_douai.manipulation_de_fonctions.operateurs.Mul;
import fr.imt_lille_douai.manipulation_de_fonctions.variables.Var;

public class Sin extends Funct1 {

	public Sin(Function f) {
		super(f);
		// TODO Auto-generated constructor stub
	}

	public double getValue() {
		return Math.sin(getF().getValue());
	}
	
	public String toString() {
		return "sin " + getF();
	}

	public Function getDiff(Var x) {
		return new Mul(f.getDiff(x), new Cos(f));
	}
}
