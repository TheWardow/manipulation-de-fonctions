package fr.imt_lille_douai.manipulation_de_fonctions.functions;

import fr.imt_lille_douai.manipulation_de_fonctions.abstracts.Funct1;
import fr.imt_lille_douai.manipulation_de_fonctions.interfaces.Function;
import fr.imt_lille_douai.manipulation_de_fonctions.operateurs.Mul;
import fr.imt_lille_douai.manipulation_de_fonctions.variables.Var;

public class Sqr extends Funct1 {

	public Sqr(Function f) {
		super(f);
		// TODO Auto-generated constructor stub
	}

	public double getValue() {
		return Math.pow(getF().getValue(), 2);
	}
	
	public String toString() {
		return "(" + getF() + ")²";
	}
	
	public Function getDiff(Var x) {
		return new Mul(f, f).getDiff(x);
	}

}
