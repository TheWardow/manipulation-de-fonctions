package fr.imt_lille_douai.manipulation_de_fonctions.functions;

import fr.imt_lille_douai.manipulation_de_fonctions.abstracts.Funct1;
import fr.imt_lille_douai.manipulation_de_fonctions.interfaces.Function;
import fr.imt_lille_douai.manipulation_de_fonctions.operateurs.Mul;
import fr.imt_lille_douai.manipulation_de_fonctions.variables.Val;
import fr.imt_lille_douai.manipulation_de_fonctions.variables.Var;

public class Sqrt extends Funct1 {

	public Sqrt(Function f) {
		super(f);
		// TODO Auto-generated constructor stub
	}

	public double getValue() {
		return Math.sqrt(getF().getValue());
	}
	
	public String toString() {
		return "sqrt(" + getF() + ")";
	}

	public Function getDiff(Var x) {
		return new Mul(f.getDiff(x),new Inv(new Mul(new Val(2.0), new Sqrt(f))));
	}
}
