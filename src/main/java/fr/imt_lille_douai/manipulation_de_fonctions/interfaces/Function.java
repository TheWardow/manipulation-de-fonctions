package fr.imt_lille_douai.manipulation_de_fonctions.interfaces;

import fr.imt_lille_douai.manipulation_de_fonctions.variables.Var;

public interface Function {
	double value = 0;
	public double getValue();
	public Function getDiff(Var x);
}
