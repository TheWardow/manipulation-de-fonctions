package fr.imt_lille_douai.manipulation_de_fonctions.operateurs;

import fr.imt_lille_douai.manipulation_de_fonctions.abstracts.Funct2;
import fr.imt_lille_douai.manipulation_de_fonctions.interfaces.Function;
import fr.imt_lille_douai.manipulation_de_fonctions.variables.Var;

public class Minus extends Funct2 {

	public Minus(Function f1, Function f2) {
		super(f1, f2);
		// TODO Auto-generated constructor stub
	}

	public double getValue() {
		return getF1().getValue() - getF2().getValue();
	}
	
	public String toString() {
		return "(" + getF1() + "-" + getF2() + ")";
	}

	public Function getDiff(Var x) {
		return new Minus(f1.getDiff(x), f2.getDiff(x));
	}

}
