package fr.imt_lille_douai.manipulation_de_fonctions.variables;

import fr.imt_lille_douai.manipulation_de_fonctions.abstracts.Funct0;
import fr.imt_lille_douai.manipulation_de_fonctions.interfaces.Function;

public class Val extends Funct0 {
	
	
	public Val(double value) {
		this.value = value;
	}

	public String toString() {
		return  "" + getValue();
	}

	public Function getDiff(Var x) {
		return new Val(0.0);
	}

}
