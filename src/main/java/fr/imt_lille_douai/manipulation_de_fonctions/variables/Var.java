package fr.imt_lille_douai.manipulation_de_fonctions.variables;

import fr.imt_lille_douai.manipulation_de_fonctions.abstracts.Funct0;
import fr.imt_lille_douai.manipulation_de_fonctions.interfaces.Function;

public class Var extends Funct0 {

	private String name;
	
	
	public Var(String name) {
		this.name = name;
	}

	
	public void setValue (double value) {
		this.value = value;
	}
	
	public String toString() {
		return  name;
	}


	public Function getDiff(Var x) {
		//return new Val(1);
		if (x.name.equals(this.name))
			return new Val(1.0);
		Var v = new Var(this.name);
		v.setValue(this.value);
		return new Val(0);
	}

}
