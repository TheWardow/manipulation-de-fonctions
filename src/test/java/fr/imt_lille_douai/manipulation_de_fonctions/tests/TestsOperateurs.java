package fr.imt_lille_douai.manipulation_de_fonctions.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.imt_lille_douai.manipulation_de_fonctions.functions.*;
import fr.imt_lille_douai.manipulation_de_fonctions.interfaces.Function;
import fr.imt_lille_douai.manipulation_de_fonctions.operateurs.*;
import fr.imt_lille_douai.manipulation_de_fonctions.variables.*;

public class TestsOperateurs {
	
	@Test
	public void testVal(){
		Function f = new Val(5);
		assertEquals(f.toString(), "5.0");
	}
	
	@Test
	public void testVar(){
		Var v = new Var("x");
		assertEquals(v.toString(), "x");
		v.setValue(10);
		assertEquals(v.toString(), "x");
		assertEquals(v.getValue(), 10.0, 0);
	}
	
	@Test
	public void testComplexFunctionsToString(){
		Var x=new Var("x"),y=new Var("y"),z=new Var("z");
		
		Function f = new Plus(x,new Mul(new Val(2),y));
		
		Function x2=new Sqr(x);
		Function y2=new Sqr(y);
		Function z2=new Sqr(z);
		Function g=new Plus(x2,new Plus(y2,z2));
		
		Function h=new Plus(new Sin(x),new Val(5));
		
		Function i=new Minus(new Sqr(new Sin(x)),new Sqr(new Cos(x)));
		
		Function j=new Mul(new Sqrt(new Sin(x)),new Inv(new Cos(x)));
		
		assertEquals("(x+(2.0*y))", f.toString());
		assertEquals("((x)²+((y)²+(z)²))", g.toString());
		assertEquals("(sin x+5.0)", h.toString());
		assertEquals("((sin x)²-(cos x)²)", i.toString());
		assertEquals("(sqrt(sin x)*1/(cos x))", j.toString());
		
	}
	
	@Test
	public void testComplexFunctionsGetValue(){
		Var x=new Var("x");
		Function k=new Plus(new Sqr(new Sin(x)),new Sqr(new Cos(x)));
		
		x.setValue(-3.0);
		assertEquals(1.0, k.getValue(), 0.001);
		x.setValue(-2.13);
		assertEquals(1.0, k.getValue(), 0.001);
		x.setValue(-1.26);
		assertEquals(1.0, k.getValue(), 0.001);
		x.setValue(-0.39);
		assertEquals(1.0, k.getValue(), 0.001);
		x.setValue(0.48);
		assertEquals(1.0, k.getValue(), 0.001);
		x.setValue(1.35);
		assertEquals(1.0, k.getValue(), 0.001);
		x.setValue(2.22);
		assertEquals(1.0, k.getValue(), 0.001);
		x.setValue(3.09);
		assertEquals(1.0, k.getValue(), 0.001);
		x.setValue(3.96);
		assertEquals(1.0, k.getValue(), 0.001);
		x.setValue(4.83);
		assertEquals(1.0, k.getValue(), 0.001);
		x.setValue(5.7);
		assertEquals(1.0, k.getValue(), 0.001);
		
	}
	
	@Test
	public void testGetDiff(){
		Var x = new Var("x");
		Function f = new Val(1.0);
		assertEquals("0.0", f.getDiff(x).toString());
		f = x;
		assertEquals("1.0", f.getDiff(x).toString());
		f = new Plus(new Val (1.0), x);
		assertEquals("(0.0+1.0)", f.getDiff(x).toString());
		f = new Minus(new Val (1.0), x);
		assertEquals("(0.0-1.0)", f.getDiff(x).toString());
		f = new Sqrt(x);
		assertEquals("(1.0*1/((2.0*sqrt(x))))", f.getDiff(x).toString());
		f = new Sqr(x);
		assertEquals("((1.0*x)+(x*1.0))", f.getDiff(x).toString());
		f = new Cos(x);
		assertEquals("(-1.0*(1.0*sin x))", f.getDiff(x).toString());
		f = new Sin(x);
		assertEquals("(1.0*cos x)", f.getDiff(x).toString());
		f = new Inv(x);
		assertEquals("(-1.0*(1.0*1/((x)²)))", f.getDiff(x).toString());
		f = new Mul(new Val(2.0), x);
		assertEquals("((0.0*x)+(2.0*1.0))", f.getDiff(x).toString());
	}
	
	@Test
	public void testgetDiffTwoVar(){
		Var x = new Var("x");
		Var y = new Var("y");
		Function f = new Mul(new Val(2), new Mul(x,y));
		Function g = new Mul(new Mul(new Val(2), x), y);
		
		System.out.println(f);
		System.out.println(g);
		
		System.out.println(f.getDiff(x));
		System.out.println(g.getDiff(x));

	}
}
